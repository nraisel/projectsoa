﻿using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.Business.Entities.MongoDb
{
    [DataContract]
    public class Serial : Entity
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<Season> Seasons { get; set; }
        [DataMember]
        public List<Actor> Actors { get; set; }
    }
}
