﻿using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.Business.Entities.MongoDb
{
    [DataContract]
    public class Episode : Entity
    {
        [DataMember]
        public int NumberOfEpisodes { get; set; }
        [DataMember]
        public Season Season { get; set; }
    }
}
