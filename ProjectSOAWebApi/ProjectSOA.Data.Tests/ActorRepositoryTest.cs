﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoRepository;
using ProjectSOA.Business.Entities.MongoDb;

namespace ProjectSOA.Data.Tests
{
    [TestClass]
    public class ActorRepositoryTest
    {
        [TestMethod]
        public void AddActorsTest()
        {
            var actor = new Actor { Name = "Patri", Nationality = "Cuban" };
            MongoRepository<Actor> _actorRepository = new MongoRepository<Actor>();
            var result = _actorRepository.Add(actor);
            Assert.IsNotNull(result);
            Assert.AreEqual("Patri", result.Name);
        }

        [TestMethod]
        public void GetActorsTest()
        {
            MongoRepository<Actor> _actorRepository = new MongoRepository<Actor>();
            var result = _actorRepository.GetEnumerator();
            result.MoveNext();
            Assert.IsNotNull(result);
            Assert.AreEqual("Ray", result.Current.Name);
        }
    }
}
