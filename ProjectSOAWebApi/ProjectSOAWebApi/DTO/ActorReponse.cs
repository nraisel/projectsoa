﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOAWebApi.DTO
{
    public class ActorReponse
    {
        public List<Actor> Actors { get; set; }
        public string ErrorMessage { get; set; }
        public ActorReponse()
        {
            Actors = new List<Actor>();
        }
    }
}
