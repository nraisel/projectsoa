﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOAWebApi.Infrastructure.Contract
{
    public interface IRepository<T>
    {
        IList<T> Get();
        T GetbyId(string Id);
        T Add(T obj);
    }
}
