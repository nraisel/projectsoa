﻿using ProjectSOAWebApi.DTO;
using ProjectSOAWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOAWebApi.Infrastructure.Contract
{
    public interface IActorRepository : IRepository<ActorReponse>
    {
    }
}
