﻿using ProjectSOAWebApi.DTO;
using ProjectSOAWebApi.Infrastructure.Contract;
using ProjectSOAWebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectSOAWebApi.Infrastructure.Realization
{
    public class ActorRepository : IActorRepository
    {
        public ActorReponse Add(ActorReponse obj)
        {
            throw new NotImplementedException();
        }

        public IList<ActorReponse> Get()
        {
            var result = new ActorReponse();
            var actorResponseList = new List<ActorReponse>();
            try {
                /*HttpResponseMessage response = new HttpResponseMessage();
                using (var client = new HttpClient())
                {
                    //client.Timeout = TimeSpan.FromMilliseconds(double.Parse(ConfigurationManager.AppSettings["TimeOut"]));
                    string uri = ConfigurationManager.AppSettings["ServiceUrl"];
                    response = client.GetAsync(uri).Result;
                }
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Actor>));
                var result = (List<Actor>)xmlSerializer.Deserialize(response.Content.ReadAsStreamAsync().Result);
                return null;*/
                
                ProxyServiceReference.CinemaServiceClient proxyService = new ProxyServiceReference.CinemaServiceClient();
                var actors = proxyService.GetJustActorsAsync().Result;
                actors.ForEach(actor => {
                    result.Actors.Add(new Actor { Id = actor.Id, Name = actor.Name, Nationality = actor.Nationality, Serials = null });
                });
                result.ErrorMessage = null;
            }
            catch (Exception ex)
            {
                result.Actors = null;
                result.ErrorMessage = ex.Message;
            }
            actorResponseList.Add(result);
            return actorResponseList;
        }

        public ActorReponse GetbyId(string Id)
        {
            throw new NotImplementedException();
        }
    }
}
