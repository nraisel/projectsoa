﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectSOAWebApi.ProxyServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Serial", Namespace="http://schemas.datacontract.org/2004/07/ProjectSOA.WcfService.ObjectModel")]
    [System.SerializableAttribute()]
    public partial class Serial : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> ActorsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Season> SeasonsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> Actors {
            get {
                return this.ActorsField;
            }
            set {
                if ((object.ReferenceEquals(this.ActorsField, value) != true)) {
                    this.ActorsField = value;
                    this.RaisePropertyChanged("Actors");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Season> Seasons {
            get {
                return this.SeasonsField;
            }
            set {
                if ((object.ReferenceEquals(this.SeasonsField, value) != true)) {
                    this.SeasonsField = value;
                    this.RaisePropertyChanged("Seasons");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Actor", Namespace="http://schemas.datacontract.org/2004/07/ProjectSOA.WcfService.ObjectModel")]
    [System.SerializableAttribute()]
    public partial class Actor : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NationalityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial> SerialsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Nationality {
            get {
                return this.NationalityField;
            }
            set {
                if ((object.ReferenceEquals(this.NationalityField, value) != true)) {
                    this.NationalityField = value;
                    this.RaisePropertyChanged("Nationality");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial> Serials {
            get {
                return this.SerialsField;
            }
            set {
                if ((object.ReferenceEquals(this.SerialsField, value) != true)) {
                    this.SerialsField = value;
                    this.RaisePropertyChanged("Serials");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Season", Namespace="http://schemas.datacontract.org/2004/07/ProjectSOA.WcfService.ObjectModel")]
    [System.SerializableAttribute()]
    public partial class Season : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Episode> EpisodesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private ProjectSOAWebApi.ProxyServiceReference.Serial SerialField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Episode> Episodes {
            get {
                return this.EpisodesField;
            }
            set {
                if ((object.ReferenceEquals(this.EpisodesField, value) != true)) {
                    this.EpisodesField = value;
                    this.RaisePropertyChanged("Episodes");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ProjectSOAWebApi.ProxyServiceReference.Serial Serial {
            get {
                return this.SerialField;
            }
            set {
                if ((object.ReferenceEquals(this.SerialField, value) != true)) {
                    this.SerialField = value;
                    this.RaisePropertyChanged("Serial");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Episode", Namespace="http://schemas.datacontract.org/2004/07/ProjectSOA.WcfService.ObjectModel")]
    [System.SerializableAttribute()]
    public partial class Episode : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int NumberOfEpisodesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private ProjectSOAWebApi.ProxyServiceReference.Season SeasonField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumberOfEpisodes {
            get {
                return this.NumberOfEpisodesField;
            }
            set {
                if ((this.NumberOfEpisodesField.Equals(value) != true)) {
                    this.NumberOfEpisodesField = value;
                    this.RaisePropertyChanged("NumberOfEpisodes");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ProjectSOAWebApi.ProxyServiceReference.Season Season {
            get {
                return this.SeasonField;
            }
            set {
                if ((object.ReferenceEquals(this.SeasonField, value) != true)) {
                    this.SeasonField = value;
                    this.RaisePropertyChanged("Season");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SerialWithActors", Namespace="http://schemas.datacontract.org/2004/07/ProjectSOA.WcfService.ObjectModel")]
    [System.SerializableAttribute()]
    public partial class SerialWithActors : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> ActorsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private ProjectSOAWebApi.ProxyServiceReference.Serial SerialField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> Actors {
            get {
                return this.ActorsField;
            }
            set {
                if ((object.ReferenceEquals(this.ActorsField, value) != true)) {
                    this.ActorsField = value;
                    this.RaisePropertyChanged("Actors");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ProjectSOAWebApi.ProxyServiceReference.Serial Serial {
            get {
                return this.SerialField;
            }
            set {
                if ((object.ReferenceEquals(this.SerialField, value) != true)) {
                    this.SerialField = value;
                    this.RaisePropertyChanged("Serial");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ProxyServiceReference.ICinemaService")]
    public interface ICinemaService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetJustSerials", ReplyAction="http://tempuri.org/ICinemaService/GetJustSerialsResponse")]
        System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial> GetJustSerials();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetJustSerials", ReplyAction="http://tempuri.org/ICinemaService/GetJustSerialsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial>> GetJustSerialsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetJustActors", ReplyAction="http://tempuri.org/ICinemaService/GetJustActorsResponse")]
        System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> GetJustActors();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetJustActors", ReplyAction="http://tempuri.org/ICinemaService/GetJustActorsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor>> GetJustActorsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetSerialWithActors", ReplyAction="http://tempuri.org/ICinemaService/GetSerialWithActorsResponse")]
        System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.SerialWithActors> GetSerialWithActors();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICinemaService/GetSerialWithActors", ReplyAction="http://tempuri.org/ICinemaService/GetSerialWithActorsResponse")]
        System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.SerialWithActors>> GetSerialWithActorsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICinemaServiceChannel : ProjectSOAWebApi.ProxyServiceReference.ICinemaService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CinemaServiceClient : System.ServiceModel.ClientBase<ProjectSOAWebApi.ProxyServiceReference.ICinemaService>, ProjectSOAWebApi.ProxyServiceReference.ICinemaService {
        
        public CinemaServiceClient() {
        }
        
        public CinemaServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CinemaServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CinemaServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CinemaServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial> GetJustSerials() {
            return base.Channel.GetJustSerials();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Serial>> GetJustSerialsAsync() {
            return base.Channel.GetJustSerialsAsync();
        }
        
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor> GetJustActors() {
            return base.Channel.GetJustActors();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.Actor>> GetJustActorsAsync() {
            return base.Channel.GetJustActorsAsync();
        }
        
        public System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.SerialWithActors> GetSerialWithActors() {
            return base.Channel.GetSerialWithActors();
        }
        
        public System.Threading.Tasks.Task<System.Collections.Generic.List<ProjectSOAWebApi.ProxyServiceReference.SerialWithActors>> GetSerialWithActorsAsync() {
            return base.Channel.GetSerialWithActorsAsync();
        }
    }
}
