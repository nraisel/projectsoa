﻿using ProjectSOAWebApi.Infrastructure.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ProjectSOAWebApi.Controllers
{
    public class ActorController : ApiController
    {
        private IActorRepository _actorRepository;
        public ActorController(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
         
        // GET: Actor
        public IHttpActionResult Get()
        {
            var result = _actorRepository.Get().FirstOrDefault();
            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
    }
}