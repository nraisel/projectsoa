﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.WcfService.ObjectModel
{
    [DataContract]
    public class Actor
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        [DataMember]
        public List<Serial> Serials { get; set; }

        public Actor()
        {

        }
    }
}
