﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProjectSOA.WcfService.ObjectModel
{
    [DataContract]
    public class Season
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Serial Serial { get; set; }
        [DataMember]
        public List<Episode> Episodes { get; set; }

        public Season()
        {

        }
    }
}