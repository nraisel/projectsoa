﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.WcfService.ObjectModel
{
    [DataContract]
    public class SerialWithActors
    {
        [DataMember]
        public Serial Serial { get; set; }
        [DataMember]
        public List<Actor> Actors { get; set; }
    }
}
