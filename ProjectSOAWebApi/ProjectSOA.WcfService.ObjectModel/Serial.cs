﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProjectSOA.WcfService.ObjectModel
{
    [DataContract]
    public class Serial
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<Season> Seasons { get; set; }
        [DataMember]
        public List<Actor> Actors { get; set; }

        public Serial()
        {

        }
    }
}