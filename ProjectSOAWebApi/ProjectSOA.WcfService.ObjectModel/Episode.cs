﻿using System.Runtime.Serialization;

namespace ProjectSOA.WcfService.ObjectModel
{
    [DataContract]
    public class Episode
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int NumberOfEpisodes { get; set; }
        [DataMember]
        public Season Season { get; set; }

        public Episode()
        {

        }
    }
}