﻿using ProjectSOA.WcfService.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectSOA.Data.Contract;
using ProjectSOA.Data.Realization;

namespace ProjectSOA.WcfService.Realization
{
    public class UnitOfWork : IUnitOfWork
    {
        public IActorRepository GetActorRespository()
        {
            return new ActorRepository();
        }

        public IEpisodeRepository GetEpisodeRespository()
        {
            return new EpisodeRepository();
        }

        public ISeasonRepository GetSeasonRespository()
        {
            return new SeasonRepository();
        }

        public ISerialRepository GetSerialRespository()
        {
            return new SerialRepository();
        }
    }
}
