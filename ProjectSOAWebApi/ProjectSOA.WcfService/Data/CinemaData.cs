﻿using ProjectSOA.WcfService.Contract;
using ProjectSOA.WcfService.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.WcfService.Data
{
    public class CinemaData
    {
        public static List<Actor> GetJustActors(IUnitOfWork uof)
        {
            try {
                var actors = new List<Actor>();
                var result = uof.GetActorRespository().GetEnumerator();
                while (result.MoveNext())
                {
                    var actor = new Actor
                    {
                        Id = result.Current.Id,
                        Name = result.Current.Name,
                        Nationality = result.Current.Nationality,
                        Serials = null
                    };
                    actors.Add(actor);
                }
                return actors;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                return null;
            }
        }

        public static List<Serial> GetJustSerials(IUnitOfWork uof)
        {
            try {
                var episodes = new List<Episode>();
                var seasons = new List<Season>();
                var serials = new List<Serial>();
                var result = uof.GetSerialRespository().GetEnumerator();
                while (result.MoveNext())
                {
                    result.Current.Seasons.ForEach(s => {
                        var season = new Season { Id = s.Id, Name = s.Name };
                        s.Episodes.ForEach(e => {
                            var episode = new Episode
                            {
                                Id = e.Id,
                                NumberOfEpisodes = e.NumberOfEpisodes,
                                Season = season
                            };
                            episodes.Add(episode);
                        });
                        season.Episodes = episodes;
                        seasons.Add(season);
                    });
                    var serial = new Serial
                    {
                        Id = result.Current.Id,
                        Name = result.Current.Name,
                        Seasons = seasons,
                        Actors = null
                    };
                    serials.Add(serial);
                }
                return serials;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                return null;
            }
        }

        public static List<SerialWithActors> GetSerialWithActors(IUnitOfWork uof)
        {
            try
            {
                var episodes = new List<Episode>();
                var seasons = new List<Season>();
                var serials = new List<Serial>();
                var actors = new List<Actor>();
                var serialWithActorsList = new List<SerialWithActors>();
                var result = uof.GetSerialRespository().GetEnumerator();
                while (result.MoveNext())
                {
                    result.Current.Seasons.ForEach(s => {
                        var season = new Season { Id = s.Id, Name = s.Name };
                        s.Episodes.ForEach(e => {
                            var episode = new Episode
                            {
                                Id = e.Id,
                                NumberOfEpisodes = e.NumberOfEpisodes,
                                Season = season
                            };
                            episodes.Add(episode);
                        });
                        season.Episodes = episodes;
                        seasons.Add(season);
                    });
                    result.Current.Actors.ForEach(a => {
                        var actor = new Actor
                        {
                            Id = a.Id,
                            Name = a.Name,
                            Nationality = a.Nationality
                        };
                        actors.Add(actor);
                    });
                    var serial = new Serial
                    {
                        Id = result.Current.Id,
                        Name = result.Current.Name,
                        Seasons = seasons,
                        Actors = actors
                    };
                    var res = new SerialWithActors { Serial = serial, Actors = serial.Actors };
                    serialWithActorsList.Add(res);
                }
                
                return serialWithActorsList;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                return null;
            }
        }
    }
}
