﻿using ProjectSOA.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.WcfService.Contract
{
    public interface IUnitOfWork
    {
        IActorRepository GetActorRespository();
        ISerialRepository GetSerialRespository();
        ISeasonRepository GetSeasonRespository();
        IEpisodeRepository GetEpisodeRespository();
    }
}
