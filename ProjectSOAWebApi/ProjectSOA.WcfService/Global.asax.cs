﻿using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Ninject;
using ProjectSOA.WcfService.Contract;
using ProjectSOA.WcfService.Realization;

namespace ProjectSOA.WcfService
{
    public class Global : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();
            return kernel;
        }
    }
}