﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectSOAWebApi.Infrastructure.Realization;

namespace ProjectSOAWebApi.Tests.Controllers
{
    /// <summary>
    /// Summary description for ActorControllerTest
    /// </summary>
    [TestClass]
    public class ActorControllerTest
    {
        [TestMethod]
        public void GetAllActorTest()
        {
            var repo = new ActorRepository();
            var result = repo.Get();
            Assert.IsNotNull(result);
        }
    }
}
