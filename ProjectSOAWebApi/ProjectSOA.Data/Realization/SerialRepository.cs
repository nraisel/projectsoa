﻿using ProjectSOA.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectSOA.Business.Entities.MongoDb;
using System.Collections;
using System.Linq.Expressions;
using MongoRepository;
using System.Configuration;

namespace ProjectSOA.Data.Realization
{
    public class SerialRepository : ISerialRepository
    {
        private MongoRepository<Serial> _Repository;
        public SerialRepository()
        {
            string cnnstring = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            _Repository = new MongoRepository<Serial>(cnnstring);
        }
        public MongoCollection<Serial> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Serial> entities)
        {
            throw new NotImplementedException();
        }

        public Serial Add(Serial entity)
        {
            return _Repository.Add(entity);
        }

        public long Count()
        {
            return _Repository.Count();
        }

        public void Delete(Serial entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<Serial, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Serial, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Serial GetById(string id)
        {
            return _Repository.GetById(id);
        }

        public IEnumerator<Serial> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Serial> entities)
        {
            throw new NotImplementedException();
        }

        public Serial Update(Serial entity)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Repository.GetEnumerator();
        }
    }
}
