﻿using ProjectSOA.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectSOA.Business.Entities.MongoDb;
using System.Collections;
using System.Linq.Expressions;
using System.Configuration;
using MongoRepository;

namespace ProjectSOA.Data.Realization
{
    public class SeasonRepository : ISeasonRepository
    {
        private MongoRepository<Season> _Repository;
        public SeasonRepository()
        {
            string cnnstring = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            _Repository = new MongoRepository<Season>(cnnstring);
        }
        public MongoCollection<Season> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Season> entities)
        {
            throw new NotImplementedException();
        }

        public Season Add(Season entity)
        {
            return _Repository.Add(entity);
        }

        public long Count()
        {
            return _Repository.Count();
        }

        public void Delete(Season entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<Season, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Season, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Season GetById(string id)
        {
            return _Repository.GetById(id);
        }

        public IEnumerator<Season> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Season> entities)
        {
            throw new NotImplementedException();
        }

        public Season Update(Season entity)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Repository.GetEnumerator();
        }
    }
}
