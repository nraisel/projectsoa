﻿using ProjectSOA.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectSOA.Business.Entities.MongoDb;
using System.Collections;
using System.Linq.Expressions;
using MongoRepository;
using System.Configuration;

namespace ProjectSOA.Data.Realization
{
    public class EpisodeRepository : IEpisodeRepository
    {
        private MongoRepository<Episode> _Repository;
        public EpisodeRepository()
        {
            string cnnstring = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            _Repository = new MongoRepository<Episode>(cnnstring);
        }
        public MongoCollection<Episode> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Episode> entities)
        {
            throw new NotImplementedException();
        }

        public Episode Add(Episode entity)
        {
            return _Repository.Add(entity);
        }

        public long Count()
        {
            return _Repository.Count();
        }

        public void Delete(Episode entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<Episode, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Episode, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Episode GetById(string id)
        {
            return _Repository.GetById(id);
        }

        public IEnumerator<Episode> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Episode> entities)
        {
            throw new NotImplementedException();
        }

        public Episode Update(Episode entity)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Repository.GetEnumerator();
        }
    }
}
