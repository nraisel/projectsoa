﻿using ProjectSOA.Data.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProjectSOA.Business.Entities.MongoDb;
using System.Collections;
using System.Linq.Expressions;
using MongoRepository;
using System.Configuration;

namespace ProjectSOA.Data.Realization
{
    public class ActorRepository : IActorRepository
    {
        private MongoRepository<Actor> _actorRepository;
        public ActorRepository()
        {
            string cnnstring = ConfigurationManager.ConnectionStrings["MongoServerSettings"].ConnectionString;
            _actorRepository = new MongoRepository<Actor>(cnnstring);
        }
        public MongoCollection<Actor> Collection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Type ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Expression Expression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(IEnumerable<Actor> entities)
        {
            throw new NotImplementedException();
        }

        public Actor Add(Actor entity)
        {
            return _actorRepository.Add(entity);
        }

        public long Count()
        {
            return _actorRepository.Count();
        }

        public void Delete(Actor entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Expression<Func<Actor, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public bool Exists(Expression<Func<Actor, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Actor GetById(string id)
        {
            return _actorRepository.GetById(id); 
        }

        public IEnumerator<Actor> GetEnumerator()
        {
            return _actorRepository.GetEnumerator();
        }

        public void RequestDone()
        {
            throw new NotImplementedException();
        }

        public IDisposable RequestStart()
        {
            throw new NotImplementedException();
        }

        public void Update(IEnumerable<Actor> entities)
        {
            throw new NotImplementedException();
        }

        public Actor Update(Actor entity)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
