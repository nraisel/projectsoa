﻿using MongoRepository;
using ProjectSOA.Business.Entities.MongoDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectSOA.Data.Contract
{
    public interface ISeasonRepository : IRepository<Season>
    {
    }
}
