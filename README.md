# README #

Project to demonstrate the use of API services and WCF, mongodba access, differente pattern designs like: Repository, DI, MVC.
In this case, we have an API Rest service which consume a WCF service hosted in IIS which (this WCF) gets information from a mongoDb collection.
Every each project is divided in layers as if it were a DDD.
The entire purpose is to use a Service Architecture Oriented Architectural Pattern so clients can make use of these services and storage layer.
Clients could be WPF, Web or Mobile applications.

Interesting things to consider here are:
  1 The use of Ninject for DI in a WCF service
  2 The use of Unity to implemente DI in a rest layer api. (The Web Api Project)
  3 The use of the dll MongoRepository to implemente the repository pattern for mongodb collections.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact